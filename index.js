import Express from 'express';
import BodyParser from 'body-parser';
import Cors from 'cors';
import Path from 'path';
import Api from './server/routers/api';
import config from './server/config';
import io from 'socket.io';
import RealtimeActivity from './server/routers/realtime/RealtimeActivityRouter';

const PORT = config.port;
const app = new Express();

const realtimeActivity = new RealtimeActivity();
//config app
app.use(Cors());
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({extended: true}));
app.use('/api', Api);

app.use(Express.static(Path.resolve(__dirname, 'server', 'public')));


//start server
const server = app.listen(PORT, () => {
    console.log(`Listen on port ${PORT}`);
});




//file upload
app.use(Express.static(Path.resolve(__dirname, 'server', 'public'), {maxAge: 31557600000}));

const  realtimeServer = io.listen(server);
realtimeServer.sockets.on('connection', (socket) => {
    realtimeActivity.onConnection(realtimeServer, socket)
})

module.exports = app;