import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import ActivityRepository from "../../respositories/ActivityRepository";
import BabyRespository from "../../respositories/BabyRespository";

import {User} from '../../models/index';

const moment = require('Moment');
import DB from '../../models';
import SolidRepository from "../../respositories/SolidRepository";
import WeightRepository from "../../respositories/WeightRepository";
import BottleRepository from "../../respositories/BottleRepository";
import DiaperRepository from "../../respositories/DiaperRepository";
import HeightRepository from "../../respositories/HeightRepository";
import SleepRepository from "../../respositories/SleepRepository";
import BreastRepository from "../../respositories/BreastRepository";

const constants = require('../../data/constant');


let sequelize = DB.sequelize;
const activityRepository = new ActivityRepository();

class ActivityController {
    update = async (req, res) => {
        try {
            let activityId = req.param('id');
            let body = req.body;
            let activity = await activityRepository.update(body, {
                where: {
                    id: activityId
                },
                individualHooks: true

            });
            console.log(activity);
            return Response.success(res, activity[1][0])
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    getActivity = async (req, res) => {
        try {
            let babyIdReq = req.param('babyId');
            let limitReq = req.param('limit');
            let offsetReq = req.param('offset')
            let activities = await  activityRepository.find({
                order: [['date', 'DESC']],
                where: {
                    babyId: babyIdReq,
                },
                limit: limitReq,
                offset: offsetReq
            });
            return Response.success(res, activities);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    }

    getLastActivity = async (req, res) => {
        try {
            let babyIdReq = req.param('babyId');
            let lastActivity = await activityRepository.find({
                where: {
                    babyId: babyIdReq
                },
                order: [['date', 'DESC']],
                limit: 1
            });
            return Response.success(res, lastActivity[0]);
        }
        catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    }


    getActivityCountByDate = async (req, res) => {
        try {
            let babyIdReq = req.param('babyId');

            let dateReq = req.param('date');
            dateReq = moment.utc(dateReq);
            let dateFrom = moment.utc(dateReq, "YYYY-MM-dd");
            console.log(dateFrom);
            let typeResult = await activityRepository.find({
                attributes: ['type'],
                where: {
                    babyId: babyIdReq,
                    date: {
                        $between: [dateFrom.format('YYYY-MM-DD HH:mm:ss'), dateFrom.add(1, 'day').format('YYYY-MM-DD HH:mm:ss')]
                    }
                },
                group: ['type']
            });
            if (typeResult.length === 0) {
                return Response.success(res, typeResult);
            }
            dateFrom = moment.utc(dateReq, "YYYY-MM-dd");
            let datTo = moment.utc(dateReq, "YYYY-MM-dd").add(1, 'day');
            let lenght = typeResult.length;
            var count = 0;
            await typeResult.forEach(async (value, i) => {
                console.log("aaa", i);
                console.log(value.dataValues, i);
                switch (value.dataValues.type) {
                    case constants.BOTTLE:
                        new BottleRepository().findOne({
                            attributes: [[sequelize.fn('SUM', sequelize.col('volume')), 'total'], [sequelize.fn('COUNT', sequelize.col('id')), 'count']],
                            where: {
                                babyId: babyIdReq,
                                timeOfFeeding: {
                                    $between: [dateFrom.format('YYYY-MM-DD HH:mm:ss'), datTo.format('YYYY-MM-DD HH:mm:ss')]
                                }
                            }
                        }).then(value2 => {
                            console.log(value2.dataValues, i);
                            value.dataValues.total = parseInt(value2.dataValues.total);
                            value.dataValues.count = parseInt(value2.dataValues.count);
                            typeResult[i] = value;
                            count++;
                            if (count === lenght) {
                                return Response.success(res, typeResult);
                            }
                        });
                        break;
                    case constants.BREAST:
                        new BreastRepository().findOne({
                            attributes: [[sequelize.fn('SUM', sequelize.col('leftDuration')), 'left'], [sequelize.fn('SUM', sequelize.col('rightDuration')), 'right'], [sequelize.fn('COUNT', sequelize.col('id')), 'count']],
                            where: {
                                babyId: babyIdReq,
                                timeOfFeeding: {
                                    $between: [dateFrom.format('YYYY-MM-DD HH:mm:ss'), datTo.format('YYYY-MM-DD HH:mm:ss')]
                                }
                            }
                        }).then(value2 => {
                            console.log(value2.dataValues, i);
                            value.dataValues.total = parseInt(value2.dataValues.left) + parseInt(value2.dataValues.right);
                            value.dataValues.count = parseInt(value2.dataValues.count);
                            typeResult[i] = value;
                            count++;
                            if (count === lenght) {
                                return Response.success(res, typeResult);
                            }
                        });
                        break;
                    case constants.SOLID:
                        let query = ' select sum("amount") as total, count("id") as "count" from  "Solid" where ' +
                            '"babyId" = ? and ' +
                            ' id in (select "typeId" from "Activity" where "type" =\'SOLID\' and "date"  between ? and ?  ) ' +
                            ' limit 1 ';
                        sequelize.query(query, {
                            replacements: [babyIdReq, dateFrom.format('YYYY-MM-DD HH:mm:ss'), datTo.format('YYYY-MM-DD HH:mm:ss')],
                            type: sequelize.QueryTypes.SELECT
                        })
                            .then(value2 => {
                                console.log(value2, i);
                                value.dataValues.total = parseInt(value2[0].total);
                                value.dataValues.count = parseInt(value2[0].count);
                                typeResult[i] = value;
                                count++;
                                if (count === lenght) {
                                    return Response.success(res, typeResult);
                                }
                            });
                        break;
                    case constants.DIAPER:
                        new DiaperRepository().findOne({
                            attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
                            where: {
                                babyId: babyIdReq,
                                timeOfChanging: {
                                    $between: [dateFrom.format('YYYY-MM-DD HH:mm:ss'), datTo.format('YYYY-MM-DD HH:mm:ss')]
                                }
                            }
                        }).then(value2 => {
                            console.log(value2.dataValues, i);
                            value.dataValues.total = parseInt(value2.dataValues.count);
                            value.dataValues.count = parseInt(value2.dataValues.count);
                            typeResult[i] = value;
                            count++;
                            if (count === lenght) {
                                return Response.success(res, typeResult);
                            }
                        });
                        break;
                    case constants.SLEEP:
                        new SleepRepository().findOne({
                            attributes: [[sequelize.fn('SUM', sequelize.col('duration')), 'total'], [sequelize.fn('COUNT', sequelize.col('id')), 'count']],
                            where: {
                                babyId: babyIdReq,
                                beginTime: {
                                    $between: [dateFrom.format('YYYY-MM-DD HH:mm:ss'), datTo.format('YYYY-MM-DD HH:mm:ss')]
                                }
                            }
                        }).then(value2 => {
                            let endMoment = new moment(value2.dataValues.endTime);
                            value.dataValues.total = endMoment.diff(value2.dataValues.beginTime, 'minutes')
                            value.dataValues.count = parseInt(value2.dataValues.count);
                            typeResult[i] = value;
                            count++;
                            if (count === lenght) {
                                return Response.success(res, typeResult);
                            }
                        });
                        break;
                    case constants.HEIGHT:
                        new HeightRepository().findOne({
                            attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
                            where: {
                                babyId: babyIdReq,
                                date: {
                                    $between: [dateFrom.format('YYYY-MM-DD HH:mm:ss'), datTo.format('YYYY-MM-DD HH:mm:ss')]
                                }
                            }
                        }).then(value2 => {
                            console.log(value2.dataValues, i);
                            value.dataValues.total = parseInt(value2.dataValues.count);
                            value.dataValues.count = parseInt(value2.dataValues.count);
                            typeResult[i] = value;
                            count++;
                            if (count === lenght) {
                                return Response.success(res, typeResult);
                            }
                        });
                        break;
                    case constants.WEIGHT:
                        new WeightRepository().findOne({
                            attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
                            where: {
                                babyId: babyIdReq,
                                date: {
                                    $between: [dateFrom.format('YYYY-MM-DD HH:mm:ss'), datTo.format('YYYY-MM-DD HH:mm:ss')]
                                }
                            }
                        }).then(value2 => {
                            console.log(value2.dataValues, i);
                            value.dataValues.total = parseInt(value2.dataValues.count);
                            value.dataValues.count = parseInt(value2.dataValues.count);
                            typeResult[i] = value;
                            count++;
                            if (count === lenght) {
                                return Response.success(res, typeResult);
                            }
                        });
                        break;
                }
            });
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    }

    delete = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let activityId = data.activityId;
            activityRepository.findOne({
                where: {
                    id: activityId
                }
            }).then(activity => {
                switch (activity.dataValues.type) {
                    case constants.BOTTLE:
                        activityRepository.delete({
                            where: {
                                id: activityId
                            }
                        }).then(value => {
                            console.log(activity.dataValues);
                            new BottleRepository().delete({
                                where: {
                                    id: activity.dataValues.typeId
                                }
                            }).then(value2 => {
                                return Response.success(res, value2);
                            }).catch(reason => {
                                console.log(reason);
                            })
                        }).catch(reason => {
                            console.log(reason);

                        });
                        break;
                    case constants.BREAST:
                        activityRepository.delete({
                            where: {
                                id: activityId
                            }
                        }).then(value => {
                            console.log(activity.dataValues);
                            new BreastRepository().delete({
                                where: {
                                    id: activity.dataValues.typeId
                                }
                            }).then(value2 => {
                                return Response.success(res, value2);
                            }).catch(reason => {
                                console.log(reason);
                            })
                        }).catch(reason => {
                            console.log(reason);

                        });
                        break;
                    case constants.SOLID:
                        activityRepository.delete({
                            where: {
                                id: activityId
                            }
                        }).then(value => {
                            console.log(activity.dataValues);
                            new SolidRepository().delete({
                                where: {
                                    id: activity.dataValues.typeId
                                }
                            }).then(value2 => {
                                return Response.success(res, value2);
                            }).catch(reason => {
                                console.log(reason);
                            })
                        }).catch(reason => {
                            console.log(reason);

                        });
                        break;
                    case constants.DIAPER:
                        activityRepository.delete({
                            where: {
                                id: activityId
                            }
                        }).then(value => {
                            console.log(activity.dataValues);
                            new DiaperRepository().delete({
                                where: {
                                    id: activity.dataValues.typeId
                                }
                            }).then(value2 => {
                                return Response.success(res, value2);
                            }).catch(reason => {
                                console.log(reason);
                            })
                        }).catch(reason => {
                            console.log(reason);

                        });
                        break;
                    case constants.SLEEP:
                        activityRepository.delete({
                            where: {
                                id: activityId
                            }
                        }).then(value => {
                            console.log(activity.dataValues);
                            new SleepRepository().delete({
                                where: {
                                    id: activity.dataValues.typeId
                                }
                            }).then(value2 => {
                                return Response.success(res, value2);
                            }).catch(reason => {
                                console.log(reason);
                            })
                        }).catch(reason => {
                            console.log(reason);

                        });
                        break;
                    case constants.HEIGHT:
                        activityRepository.delete({
                            where: {
                                id: activityId
                            }
                        }).then(value => {
                            console.log(activity.dataValues);
                            new HeightRepository().delete({
                                where: {
                                    id: activity.dataValues.typeId
                                }
                            }).then(value2 => {
                                return Response.success(res, value2);
                            }).catch(reason => {
                                console.log(reason);
                            })
                        }).catch(reason => {
                            console.log(reason);

                        });
                        break;
                    case constants.WEIGHT:
                        activityRepository.delete({
                            where: {
                                id: activityId
                            }
                        }).then(value => {
                            console.log(activity.dataValues);
                            new WeightRepository().delete({
                                where: {
                                    id: activity.dataValues.typeId
                                }
                            }).then(value2 => {
                                return Response.success(res, value2);
                            }).catch(reason => {
                                console.log(reason);
                            })
                        }).catch(reason => {
                            console.log(reason);
                        });
                        break;
                }
            });
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    getActivityAWeekAgo = async (req, res) => {
        try {
            let babyIdReq = req.param('babyId');
            let dateFrom = moment.utc(new moment(), "YYYY-MM-dd").add(-7, 'day');
            console.log(dateFrom);
            let activities = await  activityRepository.find({
                order: [['date', 'ASC']],
                where: {
                    babyId: babyIdReq,
                    date: {
                        $gte: dateFrom
                    },
                },
            });
            return Response.success(res, activities);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    }
}

export default new ActivityController();