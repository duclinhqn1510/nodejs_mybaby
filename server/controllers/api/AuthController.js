import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import AuthRepository from '../../respositories/AuthRepository';
import Model from '../../models';

const User = Model.User;

const authRepository = new AuthRepository();

class AuthController {
    login = async (req, res) => {
        console.log("login controller");
        let data = req.body;
        console.log(data);
        try {
            let {username, password} = data;
            let user = await  User.findOne({
                where: {
                    username: username,
                },
            });
            console.log(user);
            if (!user) {
                throw new Error('Not found account');
            } else if (user.validatePassword(password)) {
                let account = await authRepository.authenticate(data);
                console.log(account);
                return Response.success(res, account);
            } else {
                throw new Error('Wrong password');
            }
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    }
}

export default new AuthController();