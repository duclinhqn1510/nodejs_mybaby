import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import BabyRespository from "../../respositories/BabyRespository";

const babyRespository = new BabyRespository();

class BabyController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let result = await  babyRespository.create(data);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }
    index = async (req, res) => {
        try {
            let familyIdReq = req.param('familyId');
            console.log(familyIdReq);
            let babies = await babyRespository.find({
                where: {
                    familyId: familyIdReq
                }
            });
            return Response.success(res, babies);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    activity = async (req, res) => {

    }
}

export default new BabyController();