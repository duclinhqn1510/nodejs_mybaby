import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import BottleRepository from "../../respositories/BottleRepository";
import BabyRespository from "../../respositories/BabyRespository";

const bottleRepository = new BottleRepository();
const babyRespository = new BabyRespository();

const moment = require('Moment');
const fs = require('fs');

class BottleController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let result = await  bottleRepository.create(data);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }


    delete = async (req, res) => {
        try {
            let bottleId = req.param('id');
            let result = bottleRepository.delete({
                where: {
                    id: bottleId
                }
            });
            return Response.success(res, result);
        }
        catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    index = async (req, res) => {
        try {
            let id = req.param('babyId');
            console.log(id);
            let result = await bottleRepository.find({
                where: {
                    babyId: id
                }
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    indexById = async (req, res) => {
        try {
            let idReq = req.param('id');
            console.log(idReq);
            let result = await bottleRepository.findOne({
                where: {
                    id: idReq
                }
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    update = async (req, res) => {
        try {
            let bottleId = req.param('id');
            let body = req.body;
            console.log(bottleId);
            console.log("body:");
            console.log(body);
            let bottle = await bottleRepository.update(body, {
                where: {
                    id: bottleId
                },
                individualHooks: true
            });
            return Response.success(res, bottle[1][0])
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    getAdvice = async (req, res) => {
        try {
            let rawdata = fs.readFileSync('./MilkData.json');
            let MilkData = JSON.parse(rawdata);
            console.log(MilkData);
            let babyIdReq = req.param('babyId');
            let advice = "";
            let babyData = await babyRespository.findOne({
                where: {
                    id: babyIdReq
                }
            });
            console.log(babyData);
            let dateMoment = new moment();
            let weekDiff = dateMoment.diff(babyData.dataValues.birthday, 'weeks');
            console.log(weekDiff);
            if (weekDiff < 48) {
                for (var i = 0; i < MilkData.data.length; i++) {
                    if (weekDiff < MilkData.data[i].week) {
                        console.log(MilkData.data[i].week);
                        if (i === 0) {
                            advice = "0 -" + MilkData.data[i].week + "week of age baby should drink " + MilkData.data[i].volume + " ml " + " x " + MilkData.data[i].times + " times per day";
                        } else {
                            advice = MilkData.data[i - 1].week + "-" + MilkData.data[i].week + " week of age baby should drink " + MilkData.data[i].volume + " ml " + " x " + MilkData.data[i].times + " times per day";
                        }
                        break;
                    }
                }
            }else {
                advice = "48+ week of age baby should drink fresh milk or soy milk or yogurt 4 x times per day"
            }

            console.log(advice);
            return Response.success(res, advice)
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };


}

export default new BottleController();