import HttpStatus from 'http-status';
import Response from '../../helpers/Response';

import DeviceTokenRepository from "../../respositories/DeviceTokenRepository";
import jwt from 'jsonwebtoken';
import Path from "path";
import FS from 'fs';


const deviceTokenRepository = new DeviceTokenRepository();

class DeviceTokenController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(req.headers);
            let decoded = jwt.decode(req.headers.authorization.split(' ')[1]);
            data.userId = decoded.id;
            let result = await  deviceTokenRepository.create(data);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

}

export default new DeviceTokenController();