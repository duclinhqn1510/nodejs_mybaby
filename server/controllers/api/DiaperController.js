import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import DiaperRepository from "../../respositories/DiaperRepository";

const diaperRepository = new DiaperRepository();

class BottleController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let result = await  diaperRepository.create(data);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }
    indexById = async (req, res) => {
        try {
            let idReq = req.param('id');
            console.log(idReq);
            let result = await diaperRepository.findOne({
                where: {
                    id: idReq
                }
            });
            console.log(result);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    update = async (req, res) => {
        try {
            let idReq = req.param('id');
            let body = req.body;
            console.log(idReq);
            console.log("body:");
            console.log(body);
            let result = await diaperRepository.update(body, {
                where: {
                    id: idReq
                },
                individualHooks: true
            });
            return Response.success(res, result[1][0])
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    delete = async(req, res) => {
        try {
            let bottleId = req.param('id');
            let result = bottleRepository.delete({
                where: {
                    id: bottleId
                }
            });
            return Response.success(res, result);
        }
        catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    index = async (req, res) => {
        try {
            let id = req.param('babyId');
            console.log(id);
            let result = await bottleRepository.find({
                where: {
                    babyId: id
                }
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    activity = async (req, res) => {

    }
}

export default new BottleController();