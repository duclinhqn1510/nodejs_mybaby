import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import FamilyRespository from "../../respositories/FamilyRespository";
import UserRespository from "../../respositories/UserRespository";

const familyRespository = new FamilyRespository();
const userRespository = new UserRespository();

class FamilyController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let result = await  familyRespository.create(data);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    index = async (req, res) => {
        try {
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    indexById = async (req, res) => {
        try {
            let idReq = req.param('id');
            console.log(idReq);
            let result = await familyRespository.findOne({
                where: {
                    id: idReq
                }
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    members = async (req, res) => {
        try {
            let idReq = req.param('id');
            console.log(idReq);
            let result = await userRespository.find({
                attributes: ['username', 'fullName'],
                where: {
                    familyId: idReq
                }
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }
}

export default new FamilyController();