import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import HeightRepository from "../../respositories/HeightRepository";
import BabyRespository from "../../respositories/BabyRespository";

const heightRepository = new HeightRepository();
const babyRespository = new BabyRespository();
const constants = require('../../data/constant');
const Moment = require('Moment');
const fs = require('fs');

class HeightController {
    create = async (req, res) => {
        try {
            let rawdata = fs.readFileSync('./WHOData.json');
            let WHOData = JSON.parse(rawdata);
            let data = req.body;
            console.log(data.babyId);
            let babyData = await babyRespository.findOne({
                where: {
                    id: data.babyId
                }
            });
            let result = await  heightRepository.create(data);
            let dateMoment = new Moment(result.date);
            let monthInt = dateMoment.diff(babyData.dataValues.birthday, 'months');
            result.dataValues.diffMonth = dateMoment.diff(babyData.dataValues.birthday, 'months', true);
            console.log(result.dataValues.diffMonth);
            if (monthInt < 13) {
                let whoHeight = babyData.dataValues.gender ===  1 ? WHOData.data[monthInt].maleHeight : WHOData.data[monthInt].femaleHeight;
                console.log(Math.abs(data.number - whoHeight));
                if (data.number < whoHeight) {
                    result.dataValues.advice = "WHO BABY HEIGHT is " + whoHeight + ", your baby's height is lower than."
                } else if (data.number > whoHeight) {
                    result.dataValues.advice = "WHO BABY HEIGHT is " + whoHeight + ", your baby's height is greater than."
                } else {
                    result.dataValues.advice = "WHO BABY HEIGHT is " + whoHeight + ", your baby's height is equal than."
                }
            }
            console.log(result);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }
    update = async (req, res) => {
        try {
            let bottleId = req.param('id');
            let body = req.body;
            let comment = await heightRepository.update(body, {
                where: {
                    id: bottleId
                }
            });
            return Response.success(res, comment)
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    delete = async (req, res) => {
        try {
            let bottleId = req.param('id');
            let result = heightRepository.delete({
                where: {
                    id: bottleId
                }
            });
            return Response.success(res, result);
        }
        catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    index = async (req, res) => {
        try {
            let id = req.param('babyId');
            console.log(id);
            let babyData = await babyRespository.findOne({
                where: {
                    id: id
                }
            });
            let result = await heightRepository.find({
                where: {
                    babyId: id
                },
                order: [['date', 'ASC']]
            });
            result.forEach(value => {
                var dateMoment = new Moment(value.dataValues.date);
                value.dataValues.diffMonth = dateMoment.diff(babyData.dataValues.birthday, 'months', true);
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    activity = async (req, res) => {

    }
}

export default new HeightController();