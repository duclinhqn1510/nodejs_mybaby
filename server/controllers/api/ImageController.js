import multer from 'multer';
import HTTPStatus from 'http-status';
import Response from '../../helpers/Response';
import Path from 'path';
import uuidv4 from 'uuid/v4';


class ImageController {

    saveImage = async (req, res) => {


        let generatedId = uuidv4();

        try {
            let storage = multer.diskStorage({
                destination: (req, file, callback) => {
                    callback(null, Path.join('.', 'server', 'public', 'uploads'));
                },
                filename: (req, file, callback) => {
                    callback(null, generatedId + '.' + file.originalname.split('.').pop());
                }
            });

            let upload = multer({storage: storage}).single('photo');

            upload(req, res, (err) => {
                if (err) {
                    return Response.returnError(res, err, HTTPStatus.BAD_REQUEST)
                }
                console.log(req.file);
                let result = {
                    data: {
                        originalName: req.file.originalname,
                        generatedName: req.file.filename,
                        imageUrl: '/api/image/' + req.file.filename
                    }
                };

                Response.returnSuccess(res, result)
            });
        }
        catch (err) {
            Response.returnError(res, err, HTTPStatus.BAD_REQUEST)
        }
    };

    retrieveImage = async (req, res) => {
        console.log("retrieveImage");
        let name = req.param('name');
        return res.sendFile(Path.join(__dirname, '..', '..', 'public', 'uploads', name));
    }
}

export default new ImageController();