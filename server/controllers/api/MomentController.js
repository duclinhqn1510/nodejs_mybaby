import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import MomentRepository from "../../respositories/MomentRepository";

const momentRepository = new MomentRepository();

class MomentController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let result = await  momentRepository.create(data);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }
    index = async (req, res) => {
        try {
            let id = req.param('babyId');
            console.log(id);
            let result = await momentRepository.find({
                where: {
                    babyId: id
                }
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    activity = async (req, res) => {

    }
}

export default new MomentController();