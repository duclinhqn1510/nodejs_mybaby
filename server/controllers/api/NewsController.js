import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import NewsRepository from "../../respositories/NewsRepository";

const newsRepository = new NewsRepository();

class NewsController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let result = await  newsRepository.create(data);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    indexById = async (req, res) => {
        try {
            let idReq = req.param('id');
            console.log(idReq);
            let result = await newsRepository.findOne({
                where: {
                    id: idReq
                }
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    index = async (req, res) => {
        try {
            let result = await newsRepository.find();
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }
}
export default new NewsController();