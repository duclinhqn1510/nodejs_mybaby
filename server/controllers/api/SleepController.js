import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import SleepRepository from "../../respositories/SleepRepository";

const sleepRepository = new SleepRepository();

const Moment = require('Moment');

class SleepController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(data);
            let result = await  sleepRepository.create(data);

            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    indexById = async (req, res) => {
        try {
            let idReq = req.param('id');
            console.log(idReq);
            let result = await sleepRepository.findOne({
                where: {
                    id: idReq
                }
            });
            console.log(result);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    update = async (req, res) => {
        try {
            let idReq = req.param('id');
            let body = req.body;
            console.log(idReq);
            console.log("body:");
            console.log(body);
            let result = await sleepRepository.update(body, {
                where: {
                    id: idReq
                },
                individualHooks: true
            });
            return Response.success(res, result[1][0])
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };
}

export default new SleepController();