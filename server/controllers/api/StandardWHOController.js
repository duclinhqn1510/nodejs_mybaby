import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import StandardWHORepository from "../../respositories/StandardWHORepository";

const standardWHORepository = new StandardWHORepository();

class StandardWHOController {
    index = async (req, res) => {
        try {
            let results = await  standardWHORepository.find();
            return Response.success(res, results);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

}

export default new StandardWHOController();