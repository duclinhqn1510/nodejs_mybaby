import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import UserRepository from "../../respositories/UserRespository";
import FamilyRespository from "../../respositories/FamilyRespository";
import AuthRepository from "../../respositories/AuthRepository";

import Path from "path";
import Config from "../../config";
import JWT from 'jsonwebtoken';
import FS from 'fs';


const familyRespository = new FamilyRespository();

const userRepository = new UserRepository();
const authRepository = new AuthRepository();

class UserController {
    create = async (req, res) => {
        try {
            let data = req.body;
            console.log(req.body);
            let result;
            if (data.familyCode === "") {
                result = await  userRepository.create(data);
            } else {
                try {
                    let familyResult = await familyRespository.findOne({
                        where: {
                            code: data.familyCode
                        }
                    })
                    data.familyId = familyResult.id;
                    result = await  userRepository.create(data);
                } catch (e) {
                    return Response.error(res, e, HttpStatus.BAD_REQUEST)
                }
            }
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    update = async (req, res) => {
        try {
            let idReq = req.param('id');
            let body = req.body;
            let result = await userRepository.update(body, {
                where: {
                    id: idReq
                },
                individualHooks: true
            });
            return Response.success(res, result[1][0])
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    index = async (req, res) => {
        try {
            let result = await  userRepository.find();
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    joinFamily = async (req, res) => {
        try {
            let data = req.body;
            let familyResult = await familyRespository.findOne({
                where: {
                    code: data.familyCode
                }
            });
            if (familyResult != null) {
                data.familyId = familyResult.dataValues.id;
                console.log(data.familyId);
                let result = await userRepository.update(data, {
                    where: {
                        id: data.userId
                    },
                    individualHooks: true
                });
                return Response.success(res, result[1][0]);
            } else {
                console.log('error')
                throw Error('No family found!')
            }

        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }



}

export default new UserController();