import HttpStatus from 'http-status';
import Response from '../../helpers/Response';
import WeightRepository from "../../respositories/WeightRepository";
import BabyRespository from "../../respositories/BabyRespository";



const weightRepository = new WeightRepository();
const babyRespository = new BabyRespository();
const constants = require('../../data/constant');
const Moment = require('Moment');
const fs = require('fs');

class WeightController {
    create = async (req, res) => {
        try {
            let rawdata = fs.readFileSync('./WHOData.json');
            let WHOData = JSON.parse(rawdata);
            let data = req.body;
            console.log(data.babyId);
            let babyData = await babyRespository.findOne({
                where: {
                    id: data.babyId
                }
            });
            let result = await  weightRepository.create(data);
            let dateMoment = new Moment(result.date);
            let monthInt = dateMoment.diff(babyData.dataValues.birthday, 'months');
            result.dataValues.diffMonth = dateMoment.diff(babyData.dataValues.birthday, 'months', true);
            if (monthInt < 13) {
                let whoWeight = babyData.dataValues.gender ===  1 ? WHOData.data[monthInt].maleWeight : WHOData.data[monthInt].femaleWeight;
                console.log(Math.abs(data.number - whoWeight));
                if (data.number < whoWeight) {
                    result.dataValues.advice = "WHO BABY WEIGHT is " + whoWeight + ", your baby's weight is lower than."
                } else if (data.number > whoWeight) {
                    result.dataValues.advice = "WHO BABY WEIGHT is " + whoWeight + ", your baby's weight is greater than."
                } else {
                    result.dataValues.advice = "WHO BABY WEIGHT is " + whoWeight + ", your baby's weight is equal than."
                }
            }

            console.log(result);
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }
    update = async (req, res) => {
        try {
            let bottleId = req.param('id');
            let body = req.body;
            let comment = await weightRepository.update(body, {
                where: {
                    id: bottleId
                }
            });
            return Response.success(res, comment)
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    delete = async (req, res) => {
        try {
            let bottleId = req.param('id');
            let result = weightRepository.delete({
                where: {
                    id: bottleId
                }
            });
            return Response.success(res, result);
        }
        catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST);
        }
    };

    index = async (req, res) => {
        try {
            let id = req.param('babyId');
            console.log(id);
            let babyData = await babyRespository.findOne({
                where: {
                    id: id
                }
            });
            let result = await weightRepository.find({
                where: {
                    babyId: id
                },
                order: [['date', 'ASC']]
            });
            result.forEach(value => {
                var dateMoment = new Moment(value.dataValues.date);
                value.dataValues.diffMonth = dateMoment.diff(babyData.dataValues.birthday, 'months', true);
            });
            return Response.success(res, result);
        } catch (e) {
            return Response.error(res, e, HttpStatus.BAD_REQUEST)
        }
    }

    activity = async (req, res) => {

    }
}

export default new WeightController();