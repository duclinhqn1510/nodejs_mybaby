import DataController from './DataController';
import UserController from './UserController';
import FamilyController from './FamilyController';
import AuthController from './AuthController';
import BabyController from './BabyController';
import BottleController from './BottleController';
import HeightController from './HeightController';
import WeightController from './WeightController';
import StandardWHOController from './StandardWHOController';
import ActivityController from './ActivityController';
import SleepController from './SleepController';
import BreastController from './BreastController';
import DiaperController from './DiaperController';
import SolidController from './SolidController';
import ImageController from './ImageController';
import MomentController from './MomentController';
import DeviceTokenController from './DeviceTokenController';
import NewsController from './NewsController';

module.exports = {
    DataController,
    UserController,
    FamilyController,
    AuthController,
    BabyController,
    BottleController,
    HeightController,
    WeightController,
    StandardWHOController,
    ActivityController,
    SleepController,
    BreastController,
    DiaperController,
    SolidController,
    ImageController,
    MomentController,
    DeviceTokenController,
    NewsController
};