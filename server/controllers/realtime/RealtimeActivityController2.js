import BottleRepository from '../../respositories/BottleRepository';
import BreastRepository from '../../respositories/BreastRepository';
import DiaperRepository from '../../respositories/DiaperRepository';
import SolidRepository from '../../respositories/SolidRepository';
import SleepRepository from '../../respositories/SleepRepository';
import WeightRepository from '../../respositories/WeightRepository';
import HeightRepository from '../../respositories/HeightRepository';

import ActivityRepository from '../../respositories/ActivityRepository';
import UserRespository from '../../respositories/UserRespository';
import FamilyRespository from '../../respositories/FamilyRespository';
import DeviceTokenRepository from '../../respositories/DeviceTokenRepository';

import {Family, User} from '../../models/index';
import Response from "../../helpers/Response";

const SERVER_KEY = 'AAAAg23ohTw:APA91bG4eoNisepRS9EbLGGgt-8AJ6oikaSR7CK6ZuRI_wWg0jVxAWSS-X8XL2HbM1XvucHohbBMfju9HL-nS8jeShVxoDnFFNWQMHUf0kQXXQH2D8ywWTeP_gkwmJJsdWwZlJPluU_w';
const FCM = require('fcm-push');
const mybabyFCM = new FCM(SERVER_KEY);
import DB from '../../models';

const activityRepository = new ActivityRepository();
const userRespository = new UserRespository();
const familyRespository = new FamilyRespository();
const deviceTokenRepository = new DeviceTokenRepository();

const constants = require('../../data/constant');
let sequelize = DB.sequelize;


class RealtimeAcitivyController {
    addActivity = async (server, socket, activity) => {
        console.log(activity);
        console.log(activity);
        let typeId = 0;
        switch (activity.type) {
            case constants.BOTTLE:
                let bottle = await new BottleRepository().findOne({
                    where: {
                        id: activity.typeId
                    }
                });
                typeId = bottle.dataValues.id;
                break;
            case constants.BREAST:
                console.log("BREAST");
                let breast = await new BreastRepository().findOne({
                    where: {
                        id: activity.typeId
                    }
                });
                typeId = breast.dataValues.id;
                break;
            case constants.SOLID:
                let solid = await new SolidRepository().findOne({
                    where: {
                        id: activity.typeId
                    }
                });
                typeId = solid.dataValues.id;
                break;
            case constants.DIAPER:
                let diaper = await new DiaperRepository().findOne({
                    where: {
                        id: activity.typeId
                    }
                });
                typeId = diaper.dataValues.id;
                break;
            case constants.SLEEP:
                let sleep = await new SleepRepository().findOne({
                    where: {
                        id: activity.typeId
                    }
                });
                typeId = sleep.dataValues.id;
                break;
            case constants.HEIGHT:
                let height = await new HeightRepository().findOne({
                    where: {
                        id: activity.typeId
                    }
                });
                typeId = height.dataValues.id;
                break;
            case constants.WEIGHT:
                let weight = await new WeightRepository().findOne({
                    where: {
                        id: activity.typeId
                    }
                });
                typeId = weight.dataValues.id;
                break;
        }
        console.log(typeId);
        if (typeId !== 0) {
            activityRepository.create(activity)
                .then(function (activity) {
                    return activityRepository.findOne({
                        where: {
                            id: activity.id
                        },
                        include: [User]
                    })
                        .then(function (responseActivity) {
                            let query = 'select "token" from "DeviceToken" where "userId" in (select "id" from "User" where "familyId" = ? )';

                            sequelize.query(query, {
                                replacements: [responseActivity.familyId],
                                type: sequelize.QueryTypes.SELECT
                            }).then(function (value) {
                                console.log(value);
                                console.log(value.length);
                                let tokens = [];
                                value.forEach(value2 => {
                                    console.log(value2);
                                    tokens.push(value2.token);
                                });
                                console.log(tokens);
                                if (tokens.length > 0) {
                                    let message = {
                                        registration_ids: tokens,
                                        collapse_key: '',
                                        data: {
                                            your_custom_data_key: 'your_custom_data_value'
                                        },
                                        notification: {
                                            title: 'MyBaby',
                                            body: responseActivity.content + " " + responseActivity.date
                                        }
                                    };
                                    mybabyFCM.send(message, function (err, response) {
                                        if (err) {
                                            console.log("Something has gone wrong!");
                                        } else {
                                            console.log("Successfully sent with response: ", response);
                                        }
                                    });
                                }
                            }).catch(function (error) {
                                console.log(error)
                            });

                            server.in(responseActivity.familyId).emit("activity", responseActivity.authorId);
                        })
                        .catch(function (error) {
                            console.log(error)
                        })
                });
        } else {
            console.log("Error");
            return "Error";

        }
    };

    disconnect = async (server, socket, data) => {
        console.log('disconnect');
    };

}

export default new RealtimeAcitivyController();