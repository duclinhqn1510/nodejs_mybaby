class RealtimeFamilyController {
    joinRoom = async (server, socket, data) => {
        console.log('joined room');
        console.log(data.familyId);
        socket.join(data.familyId, function () {
            console.log("join");
        });
    }

    leaveRoom = async (server, socket, data) => {
        console.log(data);
        console.log('left room');
        if (data != null) {
            server.to(data.groupId).emit("typing", data);
        }
    }
}

export default new RealtimeFamilyController();