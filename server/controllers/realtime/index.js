import RealtimeActivityController2 from './RealtimeActivityController2';
import RealtimeFamilyController from './RealtimeFamilyController';


module.exports = {
    RealtimeActivityController2,
    RealtimeFamilyController
};