module.exports = Object.freeze({
    OK_HEIGHT: "Your baby's  is OK",
    DIFF_HEIGHT: 0.5,
    BOTTLE: "BOTTLE",
    SLEEP: "SLEEP",
    DIAPER: "DIAPER",
    WEIGHT: "WEIGHT",
    HEIGHT: "HEIGHT",
    BREAST: "BREAST",
    SOLID: "SOLID"

});