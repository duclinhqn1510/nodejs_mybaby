'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
          return queryInterface.createTable("Family", {
              id: {
                  allowNull: false,
                  primaryKey: true,
                  type: Sequelize.INTEGER,
                  autoIncrement: true

              },
              name: {
                  type: Sequelize.STRING
              },
              createdAt: {
                  allowNull: false,
                  type: Sequelize.DATE
              },
              updatedAt: {
                  allowNull: false,
                  type: Sequelize.DATE
              }

          })
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
