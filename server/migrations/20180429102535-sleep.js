'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Sleep', {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement:true
            },
            babyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Baby',
                    key: 'id'
                }
            },
            beginTime: {
                allowNull:false,
                type: Sequelize.DATE
            },
            endTime: {
                allowNull:false,
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Sleep');

    }
};
