'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Bottle', {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement:true
            },
            babyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Baby',
                    key: 'id'
                }
            },
            timeOfFeeding: {
                allowNull:false,
                type: Sequelize.DATE
            },
            volume: {
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Bottle');

    }
};
