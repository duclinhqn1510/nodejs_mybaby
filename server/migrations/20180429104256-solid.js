'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Solid', {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement:true
            },
            babyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Baby',
                    key: 'id'
                }
            },
            timeOfFeeding: {
                allowNull:false,
                type: Sequelize.DATE
            },
            amount: {
                type: Sequelize.INTEGER
            },
            babyReaction: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Solid');

    }
};
