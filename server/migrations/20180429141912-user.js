'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("User", {
            id: {
                allowNull: false,
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true

            },
            username: {
                allowNull: false,
                type: Sequelize.STRING,
                unique:true
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            },
            fullName: {
                allowNull: false,
                type: Sequelize.STRING
            },
            familyId: {
                type: Sequelize.INTEGER,
                reference: {
                    model: 'Family',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }

        })

    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.dropTable('users');
        */
    }
};
