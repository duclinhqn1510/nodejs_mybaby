
'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Activity', {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement:true
            },
            familyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Family',
                    key: 'id'
                }
            },
            babyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Baby',
                    key: 'id'
                }
            },
            authorId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            type: {
                type: Sequelize.STRING
            },
            content: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Activity');
    }
};
