'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Moment', {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement:true
            },
            babyId: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Baby',
                    key: 'id'
                }
            },
            imageUrl: {
                allowNull:false,
                type: Sequelize.STRING
            },
            caption: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Moment');

    }
};
