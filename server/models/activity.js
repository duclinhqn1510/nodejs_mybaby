'use strict';
module.exports = (sequelize, DataTypes) => {
    var Activity = sequelize.define('Activity', {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        familyId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            references: {
                model: 'Family',
                key: 'id'
            }
        },
        babyId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            references: {
                model: 'Baby',
                key: 'id'
            }
        },
        authorId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            references: {
                model: 'User',
                key: 'id'
            }
        },
        typeId: {
            allowNull: false,
            type: DataTypes.INTEGER,
        },
        type: {
            allowNull: false,
            type: DataTypes.STRING
        },
        content: {
            type: DataTypes.STRING
        },
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE
        },
        date: {
            type: DataTypes.DATE
        },
    }, {});
    Activity.associate = function (models) {
        Activity.belongsTo(models.User, {foreignKey: 'authorId'})
        Activity.belongsTo(models.Family, {foreignKey: 'familyId'})
        Activity.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
    return Activity;
};