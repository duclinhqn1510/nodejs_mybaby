'use strict';
module.exports = (sequelize, DataTypes) => {
  var Baby = sequelize.define('Baby', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      familyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Family',
              key: 'id'
          }
      },
      name: {
          allowNull: false,
          type: DataTypes.STRING,
      },
      gender: {
          allowNull: false,
          type: DataTypes.INTEGER
      },
      birthday: {
          allowNull: false,
          type: DataTypes.DATE
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Baby.associate = function (models) {
        Baby.belongsTo(models.Family, {foreignKey: 'familyId'})
    };
  return Baby;
};