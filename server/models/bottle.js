'use strict';
const Moment = require('Moment');

module.exports = (sequelize, DataTypes) => {
  var Bottle = sequelize.define('Bottle', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      timeOfFeeding: {
          allowNull:false,
          type: DataTypes.DATE
      },
      volume: {
          type: DataTypes.INTEGER
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Bottle.associate = function (models) {
        Bottle.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Bottle;
};