'use strict';
module.exports = (sequelize, DataTypes) => {
  var Breast = sequelize.define('Breast', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      timeOfFeeding: {
          allowNull:false,
          type: DataTypes.DATE
      },
      leftDuration: {
          type: DataTypes.BIGINT
      },
      rightDuration: {
          type: DataTypes.BIGINT
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Breast.associate = function (models) {
        Breast.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Breast;
};