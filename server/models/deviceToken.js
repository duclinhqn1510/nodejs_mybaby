'use strict';
module.exports = (sequelize, DataTypes) => {
  var DeviceToken = sequelize.define('DeviceToken', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      userId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'User',
              key: 'id'
          }
      },
      token: {
          allowNull: false,
          type: DataTypes.STRING,
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    DeviceToken.associate = function (models) {
        DeviceToken.belongsTo(models.User, {foreignKey: 'userId'})
    };
  return DeviceToken;
};