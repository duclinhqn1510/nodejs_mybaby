'use strict';
module.exports = (sequelize, DataTypes) => {
  var Diaper = sequelize.define('Diaper', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      timeOfChanging: {
          allowNull:false,
          type: DataTypes.DATE
      },
      reasonOfChanging: {
          allowNull:false,
          type: DataTypes.STRING
      },
      color: {
          type: DataTypes.STRING
      },
      status: {
          type: DataTypes.STRING
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Diaper.associate = function (models) {
        Diaper.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Diaper;
};