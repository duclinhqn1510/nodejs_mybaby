'use strict';
module.exports = (sequelize, DataTypes) => {
    var Family = sequelize.define('Family', {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        code: {
            allowNull: false,
            type: DataTypes.STRING
        },
        authorId: {
            type: DataTypes.INTEGER,
            references: {
                model: 'User',
                key: 'id'
            }
        }
    }, {});
    Family.associate = function (models) {
        Family.belongsTo(models.User, {foreignKey: 'authorId'})
    };
    return Family;

};

