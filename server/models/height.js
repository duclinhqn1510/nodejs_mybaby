'use strict';
module.exports = (sequelize, DataTypes) => {
  var Height = sequelize.define('Height', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      date: {
          type: DataTypes.DATE,
          allowNull: false
      },
      number: {
          type: DataTypes.FLOAT,
          allowNull: false
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Height.associate = function (models) {
        Height.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Height;
};