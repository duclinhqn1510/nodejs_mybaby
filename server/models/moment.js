'use strict';
module.exports = (sequelize, DataTypes) => {
  var Moment = sequelize.define('Moment', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      imageUrl: {
          allowNull: false,
          type: DataTypes.STRING,
      },
      caption: {
          type: DataTypes.STRING
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Moment.associate = function (models) {
        Moment.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Moment;
};