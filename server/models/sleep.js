'use strict';
module.exports = (sequelize, DataTypes) => {
  var Sleep = sequelize.define('Sleep', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      beginTime: {
          allowNull:false,
          type: DataTypes.DATE
      },
      endTime: {
          allowNull:false,
          type: DataTypes.STRING
      },
      duration: {
          allowNull:false,
          type: DataTypes.BIGINT
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Sleep.associate = function (models) {
        Sleep.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Sleep;
};