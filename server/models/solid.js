'use strict';
module.exports = (sequelize, DataTypes) => {
  var Solid = sequelize.define('Solid', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      timeOfFeeding: {
          allowNull:false,
          type: DataTypes.DATE
      },
      amount: {
          type: DataTypes.INTEGER
      },
      babyReaction: {
          type: DataTypes.STRING
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Solid.associate = function (models) {
        Solid.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Solid;
};