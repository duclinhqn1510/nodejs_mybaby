'use strict';
module.exports = (sequelize, DataTypes) => {
    var StandardWHO = sequelize.define('StandardWHO', {
        month: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        maleHeight: {
            type: DataTypes.FLOAT,
        },
        maleWeight: {
            type: DataTypes.FLOAT,
        },
        femaleHeight: {
            type: DataTypes.FLOAT,
        },
        femaleWeight: {
            type: DataTypes.FLOAT,
        }
    }, {});
    return StandardWHO;
};