'use strict';
const Bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        familyId: {
            type: DataTypes.INTEGER,
            references: {
                model: 'Family',
                key: 'id'
            }
        },
        fullName: {
            type: DataTypes.STRING
        },
        username: {
            allowNull:false,
            type: DataTypes.STRING
        },
        password: {
            allowNull:false,
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function (models) {
                User.belongsTo(models.Family, {foreignKey: 'familyId'})
            },
            generateHash(password) {
                return Bcrypt.hashSync(password, Bcrypt.genSaltSync(8), null);
            }
        },
        instanceMethods: {
            validatePassword: function (password) {
                if (Bcrypt.compareSync(password, this.password))
                    return true;
                else
                    return false;
            },
            toJSON: function () {
                let values = Object.assign({}, this.get());
                delete values.password;
                return values;
            },
        },
        hooks: {
            beforeCreate: function (user, options) {
                if (user.changed('password')) {
                    user.password = this.generateHash(user.password);
                }
            },
            beforeUpdate: function (user, options) {
                if (user.changed('password')) {
                    user.password = this.generateHash(user.password);
                }
            },
        },
        privateColumns: ['password']
    });
    return User;

};

