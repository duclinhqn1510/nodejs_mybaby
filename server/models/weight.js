'use strict';
const Moment = require('Moment');

module.exports = (sequelize, DataTypes) => {
  var Weight = sequelize.define('Weight', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      babyId: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
              model: 'Baby',
              key: 'id'
          }
      },
      number: {
          type: DataTypes.FLOAT,
          allowNull: false
      },
      date: {
          type: DataTypes.DATE,
          allowNull: false
      },
      createdAt: {
          allowNull: false,
          type: DataTypes.DATE
      },
      updatedAt: {
          allowNull: false,
          type: DataTypes.DATE
      }
  }, {});
    Weight.associate = function (models) {
        Weight.belongsTo(models.Baby, {foreignKey: 'babyId'})
    };
  return Weight;
};