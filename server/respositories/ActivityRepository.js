import BaseRepository from "./BaseRepository";

export default class ActivityRepository extends BaseRepository {
    constructor() {
        super('Activity');
    }
}