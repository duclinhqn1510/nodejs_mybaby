import   BaseRepository from  "./BaseRepository";

export  default  class DeviceTokenRepository extends  BaseRepository {
    constructor() {
        super('DeviceToken');
    }
}
