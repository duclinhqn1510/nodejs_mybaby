import BaseRepository from "./BaseRepository";

export default class MomentRepository extends BaseRepository {
    constructor() {
        super('Moment');
    }
}
