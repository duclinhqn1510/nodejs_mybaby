import   BaseRepository from  "./BaseRepository";

export  default  class NewsRepository extends  BaseRepository {
    constructor() {
        super('News');
    }
}
