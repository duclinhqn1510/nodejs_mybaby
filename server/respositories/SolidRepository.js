import BaseRepository from "./BaseRepository";

export default class BottleRepository extends BaseRepository {
    constructor() {
        super('Solid');
    }
}
