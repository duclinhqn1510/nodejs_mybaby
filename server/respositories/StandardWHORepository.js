import BaseRepository from "./BaseRepository";

export default class StandardWHORepository extends BaseRepository {
    constructor() {
        super('StandardWHO');
    }
}