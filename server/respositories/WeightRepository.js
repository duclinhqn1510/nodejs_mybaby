import   BaseRepository from  "./BaseRepository";

export  default  class WeightRepository extends  BaseRepository {
    constructor() {
        super('Weight');
    }
}
