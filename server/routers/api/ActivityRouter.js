import {Router} from 'express';
import {ActivityController, BottleController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

console.log(`FamilyRouter`);
router.route('/lastIndex').get(IsAuth.index, ActivityController.getLastActivity);
router.route('/index').get(IsAuth.index, ActivityController.getActivity);
router.route('/indexByDate').get(IsAuth.index, ActivityController.getActivityCountByDate);
router.route('/getActivityAWeekAgo').get(IsAuth.index, ActivityController.getActivityAWeekAgo);
router.route('/delete').delete(IsAuth.index, ActivityController.delete);
router.route('/:id').put(IsAuth.index,ActivityController.update);

export default router;