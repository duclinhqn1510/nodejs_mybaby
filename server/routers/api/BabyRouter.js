import {Router} from 'express';
import {BabyController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,BabyController.create);
router.route('/index').get(IsAuth.index,BabyController.index);

export default router;