import {Router} from 'express';
import {BottleController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,BottleController.create);
router.route('/index').get(IsAuth.index,BottleController.index);
router.route('/indexById').get(IsAuth.index,BottleController.indexById);
router.route('/:id').delete(IsAuth.index,BottleController.delete);
router.route('/:id').put(IsAuth.index,BottleController.update);
router.route('/advice').get(IsAuth.index,BottleController.getAdvice);

export default router;