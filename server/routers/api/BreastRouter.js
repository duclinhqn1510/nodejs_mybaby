import {Router} from 'express';
import {BottleController, BreastController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,BreastController.create);
router.route('/index').get(IsAuth.index,BreastController.index);
router.route('/:id').delete(IsAuth.index,BreastController.delete);
router.route('/:id').put(IsAuth.index,BreastController.update);
router.route('/indexById').get(IsAuth.index,BreastController.indexById);

export default router;