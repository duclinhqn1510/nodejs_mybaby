import {Router} from 'express';
import {DeviceTokenController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,DeviceTokenController.create);

export default router;