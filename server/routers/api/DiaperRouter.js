import {Router} from 'express';
import {BreastController, DiaperController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,DiaperController.create);
router.route('/index').get(IsAuth.index,DiaperController.index);
router.route('/:id').delete(IsAuth.index,DiaperController.delete);
router.route('/:id').put(IsAuth.index,DiaperController.update);
router.route('/indexById').get(IsAuth.index,DiaperController.indexById);

export default router;