import {Router} from 'express';
import {FamilyController, UserController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`FamilyRouter`);
router.route('/create').post(FamilyController.create);
router.route('/index').get(IsAuth.index,FamilyController.index);
router.route('/indexById').get(IsAuth.index,FamilyController.indexById);
router.route('/members').get(IsAuth.index,FamilyController.members);

export default router;