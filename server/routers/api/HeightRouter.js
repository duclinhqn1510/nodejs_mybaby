import {Router} from 'express';
import {HeightController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
router.route('/create').post(IsAuth.index,HeightController.create);
router.route('/index').get(IsAuth.index,HeightController.index);
router.route('/:id').delete(IsAuth.index,HeightController.delete);
router.route('/:id').put(IsAuth.index,HeightController.update);

export default router;