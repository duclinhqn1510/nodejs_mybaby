import {Router} from 'express';
import {MomentController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,MomentController.create);
router.route('/index').get(IsAuth.index,MomentController.index);

export default router;