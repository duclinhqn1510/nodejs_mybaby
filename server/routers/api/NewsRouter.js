import {Router} from 'express';
import {NewsController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

router.route('/create').post(IsAuth.index,NewsController.create);
router.route('/index').get(IsAuth.index,NewsController.index);

export default router;