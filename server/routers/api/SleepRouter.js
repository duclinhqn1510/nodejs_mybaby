import {Router} from 'express';
import {SleepController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,SleepController.create);
router.route('/:id').put(IsAuth.index,SleepController.update);
router.route('/indexById').get(IsAuth.index,SleepController.indexById);
export default router;