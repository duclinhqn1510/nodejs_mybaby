import {Router} from 'express';
import {BreastController, SolidController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`FamilyRouter`);
router.route('/create').post(IsAuth.index,SolidController.create);
router.route('/index').get(IsAuth.index,SolidController.index);
router.route('/:id').delete(IsAuth.index,SolidController.delete);
router.route('/:id').put(IsAuth.index,SolidController.update);
router.route('/indexById').get(IsAuth.index,SolidController.indexById);

export default router;