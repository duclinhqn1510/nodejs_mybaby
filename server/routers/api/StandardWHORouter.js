import {Router} from 'express';
import {StandardWHOController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

router.route('/index').get(IsAuth.index,StandardWHOController.index);

export default router;