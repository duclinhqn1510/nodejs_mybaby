import {Router} from 'express';
import {UserController,AuthController } from '../../controllers/api';

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
console.log(`news router`);
router.route('/create').post(UserController.create);
router.route('/index').get(UserController.index);
router.route('/login').post(AuthController.login);
router.route('/:id').put(UserController.update);
router.route('/joinFamily').post(UserController.joinFamily);

export default router;