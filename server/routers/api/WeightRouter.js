import {Router} from 'express';
import {WeightController} from '../../controllers/api';
import {IsAuth} from "../../middlewares";

const router = Router();

// router.route('/viewByUserId').get(NewsController.index);
router.route('/create').post(IsAuth.index,WeightController.create);
router.route('/index').get(IsAuth.index,WeightController.index);
router.route('/:id').delete(IsAuth.index,WeightController.delete);
router.route('/:id').put(IsAuth.index,WeightController.update);

export default router;