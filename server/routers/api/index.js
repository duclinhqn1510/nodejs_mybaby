import {Router} from 'express';
import UserRouter from "../../routers/api/UserRouter";
import FamilyRouter from "../../routers/api/FamilyRouter";
import BabyRouter from "../../routers/api/BabyRouter";
import BottleRouter from "../../routers/api/BottleRouter";
import WeightRouter from "../../routers/api/WeightRouter";
import StandardWHORouter from "../../routers/api/StandardWHORouter";
import ActivityRouter from "../../routers/api/ActivityRouter";
import SleepRouter from "../../routers/api/SleepRouter";
import BreastRouter from "../../routers/api/BreastRouter";
import DiaperRouter from "../../routers/api/DiaperRouter";
import SolidRouter from "../../routers/api/SolidRouter";
import ImageRouter from "../../routers/api/ImageRouter";
import MomentRouter from "../../routers/api/MomentRouter";
import HeightRouter from "../../routers/api/HeightRouter";
import DeviceTokenRouter from "../../routers/api/DeviceTokenRouter";
import NewsRouter from "../../routers/api/NewsRouter";

const router = Router();

router.use('/user', UserRouter);
router.use('/family', FamilyRouter);
router.use('/baby', BabyRouter);
router.use('/bottle', BottleRouter);
router.use('/weight', WeightRouter);
router.use('/height', HeightRouter);
router.use('/standardWHO', StandardWHORouter);
router.use('/activity', ActivityRouter);
router.use('/sleep', SleepRouter);
router.use('/breast', BreastRouter);
router.use('/diaper', DiaperRouter);
router.use('/solid', SolidRouter);
router.use('/image', ImageRouter);
router.use('/moment', MomentRouter);
router.use('/deviceToken', DeviceTokenRouter);
router.use('/news', NewsRouter);

console.log(`router `);
export default router; 