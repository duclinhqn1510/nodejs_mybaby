import {RealtimeActivityController2, RealtimeFamilyController} from  '../../controllers/realtime';

export default class RealtimeActivityRouter {

    onConnection = (realtimeServer, socket ) => {
        socket.on('send', (data) => {
           return RealtimeActivityController2.addActivity(realtimeServer, socket, data)
        });


        socket.on('join_room', (room) => {
            return RealtimeFamilyController.joinRoom(realtimeServer, socket, room)
        });

        socket.on('leave_room', (room) => {
            return RealtimeFamilyController.leaveRoom(realtimeServer, socket, room);
        });
        socket.on('disconnect', (data) => {
            return RealtimeActivityController2.disconnect(realtimeServer, socket, data)
        });
    }


}